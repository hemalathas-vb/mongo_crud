var http = require('http'); 
var MongoClient = require('mongodb').MongoClient; 
var url = "mongodb://localhost:27017/employee_db"; 
MongoClient.connect(url, function(err, client) { 
const db = client.db("employee_db");
if (err) throw err; 
var prevData = {'emp_name':"Arpita"}
var updatedData = {$set: {"emp_name":"Kiran"}}
db.collection("employee").updateOne(prevData, updatedData, function(err, result) {
if (err) throw err; 
console.log(result); 
client.close(); 
}); 
});