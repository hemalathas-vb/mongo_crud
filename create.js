var MongoClient = require("mongodb").MongoClient;
 var url = "mongodb://localhost:27017/employee_db";
 MongoClient.connect(url, function (err, client) {
   if (err) throw err;
   var myobj = [
       {"emp_id":01,"emp_name":"Hema"},
       {"emp_id":02,"emp_name":"Abhi"},
       {"emp_id":03,"emp_name":"Arpita"},
       {"emp_id":04,"emp_name":"Radika"},
       {"emp_id":05,"emp_name":"Priya"},
       {"emp_id":06,"emp_name":"Ashok"},
       {"emp_id":07,"emp_name":"Manju"},
       {"emp_id":08,"emp_name":"Akash"},
       {"emp_id":09,"emp_name":"Jaya"},
       {"emp_id":10,"emp_name":"Chetan"}
   ];
   const db = client.db("employee_db");
   db.collection("employee").insertMany(myobj, function (err, res) {
     if (err) throw err;
     console.log("Number of records inserted: " + res.insertedCount);
     client.close();
   });
 });